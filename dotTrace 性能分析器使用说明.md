> 📃版本: 1.0
>
> 📆日期: 2021-12-14

[TOC]



# dotTrace 性能分析器使用说明

## 1 简介

Jetbrains dotTrace是一款.NET应用程序性能分析器。

可以帮助您检测各种 .NET 和 .NET Core 应用程序的性能瓶颈：WPF 与通用 Windows 平台、ASP.NET、Windows 服务、WCF 服务和单元测试。 还支持 Mono 和 Unity 应用程序。

>**引用**
>
>[中文官网](https://www.jetbrains.com/zh-cn/profiler/)
>
>[官方说明文档（英文）](https://www.jetbrains.com/help/profiler/Introduction.html)

## 2 演示：使用dotTrace对M3客户端收集性能日志

### 2.1 打开dotTrace软件。

<img src="https://gitee.com/ChanHowe/document/raw/master/%E5%9B%BE%E5%BA%8A/JetBrains%20dotTrace/step_1.png" alt="步骤1"  />	

### 2.2 新建分析

![](https://gitee.com/ChanHowe/document/raw/master/%E5%9B%BE%E5%BA%8A/JetBrains%20dotTrace/step_2_3_4.png)

![](https://gitee.com/ChanHowe/document/raw/master/%E5%9B%BE%E5%BA%8A/JetBrains%20dotTrace/step_5_6_7.png)

![](https://gitee.com/ChanHowe/document/raw/master/%E5%9B%BE%E5%BA%8A/JetBrains%20dotTrace/step_8.png)

### 2.3 在M3中进行性能问题重现操作。

### 2.4 重现操作完成后，保存分析文件。

![](https://gitee.com/ChanHowe/document/raw/master/%E5%9B%BE%E5%BA%8A/JetBrains%20dotTrace/step_9_10.png)

![](https://gitee.com/ChanHowe/document/raw/master/%E5%9B%BE%E5%BA%8A/JetBrains%20dotTrace/step_11.png)