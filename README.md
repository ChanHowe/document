# Document

#### 介绍

Hello,这里是🚑的共享文档。😃

### 📌 强烈推介新文档站点，全新UI体验，加量不加价，童叟无欺。
👍👍👍 [fschenzihao.github.io/document/](https://fschenzihao.github.io/document/) 🎉🎉🎉  

#### 目录结构

1.  SQL Server Profiler 模板
2.  图床 *(存放图片的目录)*
3.  。。。

#### 文档列表

1.  [SQL Server 性能相关](https://gitee.com/ChanHowe/document/blob/master/SQL%20Server%20%E6%80%A7%E8%83%BD%E7%9B%B8%E5%85%B3.md)
2.  [SQL Server Profiler（数据库跟踪）使用说明](https://gitee.com/ChanHowe/document/blob/master/SQL%20Server%20Profiler%EF%BC%88%E6%95%B0%E6%8D%AE%E5%BA%93%E8%B7%9F%E8%B8%AA%EF%BC%89%E4%BD%BF%E7%94%A8%E8%AF%B4%E6%98%8E.md)
3.  [SQL Server 变更数据捕获（CDC）](https://gitee.com/ChanHowe/document/blob/master/SQL%20Server%20%E5%8F%98%E6%9B%B4%E6%95%B0%E6%8D%AE%E6%8D%95%E8%8E%B7%EF%BC%88CDC%EF%BC%89.md)
3.  [dotTrace 性能分析器使用说明](https://gitee.com/ChanHowe/document/blob/master/dotTrace%20%E6%80%A7%E8%83%BD%E5%88%86%E6%9E%90%E5%99%A8%E4%BD%BF%E7%94%A8%E8%AF%B4%E6%98%8E.md)
3.  [Windows Server 系统的性能分析](https://gitee.com/ChanHowe/document/blob/master/Windows%20Server%20%E6%80%A7%E8%83%BD%E5%88%86%E6%9E%90.md)

​		
